
import sys
from sets import Set
import random
import copy
import operator

t = 0
s = None

class Graph:
    def __init__(self, numNodes):
        self.nodes = []

        for i in xrange(numNodes):
            self.nodes.append(Node())

    def addEdge(self, headInd, tailInd):
        self.nodes[headInd].addAdjNode(self.nodes[tailInd])

    def getNodes(self):
        return self.nodes

    def sortNodesByFt(self):
        self.nodes.sort(reverse=True)

class Edge:
    def __init__(self, firstNode, secondNode):
            self.first = firstNode
            self.second = secondNode

    def getFirst(self):
        return self.first

    def getSecond(self):
        return self.second

class Node:
    def __init__(self):
        self.adjNodes = []
        self.explored = False
        self.ft = 0
        self.leaderNode = None

    def addAdjNode(self, node):
        self.adjNodes.append(node)

    def getAdjNodes(self):
        return self.adjNodes

    def setExplored(self):
        self.explored = True

    def isExplored(self):
        return self.explored

    def setFinishingTime(self, ft):
        self.ft = ft

    def getFinishingTime(self):
        return self.ft

    def setLeader(self, leaderNode):
        self.leaderNode = leaderNode

    def getLeader(self):
        return self.leaderNode

    def __lt__(self, other):
        return self.ft < other.ft


def dfs(node):
    global t, s

    stack = [[node, 0]]
    
    while stack:
        n = stack[-1][0]
        ind = stack[-1][1]
        n.setExplored()
        n.setLeader(s)

        if ind >= len(n.getAdjNodes()):
            t += 1
            n.setFinishingTime(t)
            stack.pop()
            continue
        
        for i in xrange(ind, len(n.getAdjNodes())):
            stack[-1][1] = i + 1        
            neighbor = n.getAdjNodes()[i]

            if not neighbor.isExplored():
                stack.append([neighbor,0])
                break


def dfsLoop(graph):
    global t, s

    t = 0
    s = None
    
    nodesNum = len(graph.getNodes())
    cnt = 0

    for node in graph.getNodes():
        cnt += 1
        if cnt % 100:
            sys.stdout.write("\rDone: %.2f%%" % (100 * cnt / nodesNum))
            sys.stdout.flush()
        if not node.isExplored():
            s = node
            dfs(node)

    print

def readGraphFromFile(graph, revGraph, fileName):
    print "Reading graph..."
    
    with open(fileName, 'r') as f:
        for line in f:
            strNodes = line.strip().split()
            headInd = int(strNodes[0]) - 1
            tailInd = int(strNodes[1]) - 1
           
            graph.addEdge(headInd, tailInd)
            revGraph.addEdge(tailInd, headInd)

    print "Graph read"


def copyFinishingTimes(revGraph, graph):
    
    for i in xrange(len(revGraph.getNodes())):
        graph.getNodes()[i].setFinishingTime(revGraph.getNodes()[i].getFinishingTime())

def countScc(nodes):
    sccDict = dict()

    for node in nodes:
        leader = node.getLeader()
        if leader in sccDict:
            sccDict[leader] += 1
        else:
            sccDict[leader] = 1

    sortedSccs = sorted(sccDict.items(), key=operator.itemgetter(1), reverse=True)   

    del sccDict
    result = []

    sortedSccsLen = len(sortedSccs)

    for i in xrange(5):
        if i >= sortedSccsLen:
            result.append(0)
        else:
            result.append(sortedSccs[i][1])

    return result

def printResult(result):
    resultStr = "Result: "
    for item in result:
        resultStr += str(item) + ", "

    print resultStr


if __name__ == "__main__":
    size = int(sys.argv[1])

    sys.setrecursionlimit(10000)
    
    print "Creating graph ..."
    graph = Graph(size)
    print "Creating revGraph ..."
    revGraph = Graph(size)
    readGraphFromFile(graph, revGraph, sys.argv[2])

    print "Running first  dfsLoop ..."
    dfsLoop(revGraph) 
    print "Copying finishing times ..."
    copyFinishingTimes(revGraph, graph)
    del revGraph
    print "Sorting nodes by finishing time ..."
    graph.sortNodesByFt()
    print "Running second dfsLoop ..."
    dfsLoop(graph)
    print "Counting sccs ..."
    result = countScc(graph.getNodes())
    printResult(result)



