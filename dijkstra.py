import sys
from sets import Set
import heapq 
from itertools import count

class Graph:
    def __init__(self, numNodes):
        self.nodes = []

        for i in xrange(numNodes):
            self.nodes.append(Node(i + 1))

    def addEdge(self, headInd, tailInd, weight):
        self.nodes[headInd].addAdjNode(self.nodes[tailInd], weight)

    def getNodes(self):
        return self.nodes

    def calculateShortestPaths(self, sourceNodeInd):
        print "Calculating shortest paths ..."
        self.nodes[sourceNodeInd].setPathLen(0)

        unexploredNodes = NodeHeap(self.nodes)
        nodesNum = len(unexploredNodes)
        cnt = 0

        explored = Set([])

        while not unexploredNodes.isEmpty():
            node = unexploredNodes.pop()
            cnt += 1

            sys.stdout.write("\rDone: %.2f%%" % (100 * cnt / nodesNum))
            sys.stdout.flush()
 
            for adjNode, weight in node.getAdjNodesAndWeights():
                if unexploredNodes.isNodeInside(adjNode):
                    newLenEstimate = node.getPathLen() + weight
                    
                    if newLenEstimate < adjNode.getPathLen():
                        unexploredNodes.remove(adjNode)
                        adjNode.setPathLen(newLenEstimate)
                        unexploredNodes.push(adjNode)


        print

class NodeHeap:
    REMOVED = "REMOVED"
    def __init__(self, nodes):
        self.queue = []
        self.nodeFinder = dict()
        self.counter = count()

        for node in nodes:
            self.push(node)

    def __len__(self):
        return len(self.nodeFinder)

    def push(self, node):
        if node in self.nodeFinder.keys():
            self.remove(node)
        
        cnt = next(self.counter)
        lenEstimate = node.getPathLen()
        entry = [lenEstimate, cnt, node]
        self.nodeFinder[node] = entry
        heapq.heappush(self.queue, entry)

    def remove(self, node):
        entry = self.nodeFinder.pop(node)
        entry[-1] = NodeHeap.REMOVED


    def pop(self):
        while self.queue:
            lenEstimate, cnt, node = heapq.heappop(self.queue)

            if node is not NodeHeap.REMOVED:
                del self.nodeFinder[node]
                return node

        raise KeyError("Pop from empty node heap")

    def isEmpty(self):
        return len(self.nodeFinder) == 0

    def isNodeInside(self, node):
        return node in self.nodeFinder

class Edge:
    def __init__(self, firstNode, secondNode, weight):
            self.first = firstNode
            self.second = secondNode
            self.weight = weight

    def __hash__(self):
        return hash(self.first) + hash(self.second)

    def __eq__(self, other):
        if self.first == other.first and self.second == other.second:
            return True

        elif self.second == other.first and self.first == other.second:
            return True
        
        return False

    def __ne__(self, other):
        return not self.__eq__()

    def getFirst(self):
        return self.first

    def getSecond(self):
        return self.second

    def getWeight(self):
        return self.weight

class Node:
    INFINITE_LENGTH = 1000000
    
    def __init__(self, index):
        self.index = index
        self.adjEdges = Set([])
        self.pathLen = Node.INFINITE_LENGTH 

    def addAdjNode(self, node, weight):
        newEdge = Edge(self, node, weight)
        self.adjEdges.add(newEdge)
        node.adjEdges.add(newEdge)

    def getAdjEdges(self):
        return self.adjEdges

    def getAdjNodesAndWeights(self):
        for edge in self.adjEdges:
            if edge.getFirst() != self:
                yield (edge.getFirst(), edge.getWeight())
            elif edge.getSecond() != self:
                yield (edge.getSecond(), edge.getWeight())

    def setPathLen(self, pathLen):
        self.pathLen = pathLen

    def getPathLen(self):
        return self.pathLen

def readGraphFromFile(graph, fileName):
    print "Reading graph..."
    
    with open(fileName, 'r') as f:
        for line in f:
            strElements = line.strip().split()
            firstNodeInd = int(strElements[0]) - 1
            
            for i in xrange(1, len(strElements)):
                nodeWeightPair = strElements[i].strip().split(',')
                secondNodeInd = int(nodeWeightPair[0]) - 1
                weight = int(nodeWeightPair[1])
                graph.addEdge(firstNodeInd, secondNodeInd, weight)

    print "Graph read"


def printResult(resultNodes, outputNodes):
    resultStr = "Result: "

    if outputNodes == "ALL":
        for node in resultNodes:
            resultStr += str(node.getPathLen()) + ","
    else:
        for nodeInd in outputNodes:
            resultStr += str(resultNodes[nodeInd - 1].getPathLen()) + ","

    print resultStr

def printHelp():
    print "Usage:"
    print "<command_name> [number_of_nodes] [input_file_name] [source_node] [output_nodes]"
    print "[output_nodes] - comma-separated list of nodes or \"ALL\" literal"


def main():
    if len(sys.argv) < 4:
        printHelp()
        return

    outputNodes = "ALL"

    if sys.argv[4] != "ALL":
        outputNodes = list(map(int, sys.argv[4].strip().split(',')))

    size = int(sys.argv[1])
    sourceNodeInd = int(sys.argv[3]) - 1    
    fileName = sys.argv[2]

    print "Creating graph ..."
    graph = Graph(size)
    readGraphFromFile(graph, fileName)
    graph.calculateShortestPaths(sourceNodeInd)
    resultNodes = graph.getNodes()
    printResult(resultNodes, outputNodes)




if __name__ == "__main__":
    main()



