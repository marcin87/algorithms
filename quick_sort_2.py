
import sys

comparisons = 0


def partition(A, beg, end):
    A[beg], A[end - 1] = A[end - 1], A[beg]
    p = A[beg]
    i = beg + 1

    for j in xrange(beg + 1, end):
        if A[j] < p:
            A[i], A[j] = A[j], A[i]
            i += 1
   
    A[beg], A[i - 1] = A[i - 1], A[beg]
   
    return i - 1

def quickSort(A, beg, end):
    global comparisons

    if end - beg <= 1:
        return

    comparisons += end - beg - 1


    p = partition(A, beg, end)

    quickSort(A, beg, p)
    quickSort(A, p + 1, end)



if __name__ == "__main__":
    A = []

    with open(sys.argv[1], 'r') as f:
        for line in f:
            A.append(int(line.strip()))

    quickSort(A, 0, len(A))

    print comparisons

