
import sys

def loadArray(file):
    array = []
    i = 0
    for line in file:
        array.append(int(line.strip()))
    
    return array

def countSplitInv(B, C):
    i = 0
    j = 0
    D = []
    invCnt = 0
    bLen = len(B)
    cLen = len(C)
    n = bLen + cLen

    for k in xrange(n):
        if i >= bLen:
            while j < cLen:
                D.append(C[j])
                j += 1
            break
        
        if j >= cLen:
            while i < bLen:
                D.append(B[i])
                i += 1
            break

        if B[i] <= C[j]:
            D.append(B[i])
            i += 1
        else:
            D.append(C[j])
            j += 1
            invCnt += n/2 - i

    return (D, invCnt)


def sortAndCount(A):
    n = len(A)
    if n <= 1:
        return (A, 0)
    
    B, x = sortAndCount(A[:n/2])
    C, y = sortAndCount(A[n/2:])
    D, z = countSplitInv(B, C)
    return (D, x + y + z)

if __name__ == "__main__":
    f = open(sys.argv[1])
    integers = loadArray(f)
    A, n = sortAndCount(integers)
    
    print "Inversions: " + str(n)
