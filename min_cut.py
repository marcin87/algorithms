
import sys
from sets import Set
import random
import copy

class Graph:
    def __init__(self, numVertices):
        self.edges = Set([])
        self.vertices = []

        for i in xrange(numVertices):
            self.vertices.append(Vertex(i))

    def addEdge(self, indFirst, indSecond):
        if indFirst > len(self.vertices) or indSecond > len(self.vertices):
            raise Exception("Graph.addEdge: vertex index exceeds vertices list size")
        
        self.edges.add(Edge(self.vertices[indFirst], self.vertices[indSecond]))

    def printAllEdges(self):
        print "All edges: "
        for edge in self.edges:
            edge.printEdge(),

    def contractEdge(self):
        edge = random.sample(self.edges, 1)[0]

        #edge.printEdge()
        removedVertex = edge.getSecondVertex()
        self.edges -= edge.contract()
        self.edges.discard(edge)
       
        edgesToRemove = Set([])
        
        for e in self.edges:
            if e.getFirstVertex() == removedVertex or \
            e.getSecondVertex() == removedVertex:
                edgesToRemove.add(e)

        self.edges -= edgesToRemove
        #self.printAllEdges()

    def findMinCut(self):
        vertCount = len(self.vertices)
        
        while vertCount > 2:
            self.contractEdge()
            vertCount -= 1
            #print "vertCount: ", vertCount
   
        return len(self.edges)

class Vertex:
    def __init__(self, index):
        self.adjEdges = Set([])
        self.index = index

    def addAdjEdge(self, edge):
        self.adjEdges.add(edge)

    def getAdjEdges(self):
        return self.adjEdges

    def removeAdjEdge(self, edge):
        self.adjEdges.remove(edge)

    def __eq__(self, other):
        return self.index == other.index

class Edge:
    def __init__(self, firstVertex, secondVertex):
        self.first = firstVertex
        self.second = secondVertex

        self.first.addAdjEdge(self)
        self.second.addAdjEdge(self)

    def getFirstVertex(self):
        return self.first

    def getSecondVertex(self):
        return self.second

    def setFirstVertex(self, vertex):
        self.first = vertex

    def setSecondVertex(self, vertex):
        self.second = vertex

    def contract(self):
        selfLoops = Set([])
        #print "Contract..."
        for edge in self.second.getAdjEdges():
            #print "AdjEdge:"
            #edge.printEdge()
            
            if edge.getFirstVertex() == self.first or edge.getSecondVertex() == self.first:
                self.first.removeAdjEdge(edge) 
                selfLoops.add(edge)
                #print "selfLoop"
                continue
 
            if edge.getFirstVertex() == self.second:
                edge.setFirstVertex(self.first)
                #print "setFirstVertex"
            elif edge.getSecondVertex() == self.second:
                edge.setSecondVertex(self.first)
                #print "setSecondVertex"
            else:
                #print "ERROR: index ", self.second.index
                self.printEdge
                continue

            self.first.addAdjEdge(edge)
            #print "addAdjEdge"

        self.second.getAdjEdges().clear()

        return selfLoops


    def printEdge(self):
        print "EDGE(",self.first.index + 1, ", ", self.second.index + 1, ")"




def readGraphFromFile(graph, fileName):
    print "Reading graph..."
    
    with open(fileName, 'r') as f:
        for line in f:
            strVerts = line.strip().split()
            vertInd = int(strVerts[0]) - 1
            for i in xrange(1, len(strVerts)):
                adjInd = int(strVerts[i]) - 1

                if adjInd > vertInd:
                    graph.addEdge(vertInd, adjInd)        

    print "Graph read"

if __name__ == "__main__":
    size = int(sys.argv[1])

    sys.setrecursionlimit(10000)
    graph = Graph(size)
    readGraphFromFile(graph, sys.argv[2])

    maxIters = size

    minCut = 10000000
    
    for i in xrange(maxIters):
        print i
        tempGraph = copy.deepcopy(graph)
        curMinCut = tempGraph.findMinCut()
        
        if curMinCut < minCut:
            minCut = curMinCut

    print "Min cut:", minCut




