import sys


class EmptyHeapExcetion(Exception):
    pass

class BaseHeap:
    def __init__(self):
        self.array = []

    def insert(self, elem):
        self.array.append(elem)
        
        currInd = self.size() - 1
        parentInd = self.parent(currInd)
        
        while currInd > 0:
            if self.isBubbleUpNeeded(self.array[currInd], self.array[parentInd]):
                self.array[currInd],  self.array[parentInd] = \
                    self.array[parentInd], self.array[currInd]
                currInd = parentInd
                parentInd = self.parent(currInd)
            else:
                return

    def getFirst(self):
        if self.size() > 0:
            return self.array[0]

    def extractFirst(self):
        if self.size() == 0:
            raise EmptyHeapExcetion("Cannot extract an element from an empty heap")

        lastInd = self.size() - 1
        self.array[0],  self.array[lastInd] = \
            self.array[lastInd], self.array[0]

        extractedElem = self.array.pop()

        
        
        currInd = 0
        lChild = self.leftChild(currInd)
        rChild = self.rightChild(currInd)

        while True:
            resInd = self.bubbleDown(currInd, lChild, rChild)

            if resInd == currInd:
                break

            currInd = resInd
            lChild = self.leftChild(currInd)
            rChild = self.rightChild(currInd)


        return extractedElem
            


    def isBubbleUpNeeded(self, currKey, parentKey):
        pass

    def bubbleDown(self, currInd, lChildInd, rChildInd):
        pass

    def parent(self, i):
        return i // 2

    def leftChild(self, i):
        if i == 0:
            return 1

        return 2 * i

    def rightChild(self, i):
        if i == 0:
            return 2

        return 2 * i + 1

    def size(self):
        return len(self.array)

   

class MinHeap(BaseHeap):
    def isBubbleUpNeeded(self, currKey, parentKey):
        return currKey < parentKey

    def bubbleDown(self, currInd, lChildInd, rChildInd):
        smallestInd = currInd

        if lChildInd >= self.size():
            return currInd
        elif rChildInd >= self.size():
            smallestInd = lChildInd
        else:
            smallestInd = lChildInd if self.array[lChildInd] < self.array[rChildInd] else rChildInd

        if self.array[currInd] > self.array[smallestInd]:
            self.array[currInd], self.array[smallestInd] = \
                self.array[smallestInd], self.array[currInd]
        else:
            smallestInd = currInd

        return smallestInd



class MaxHeap(BaseHeap):
    def isBubbleUpNeeded(self, currKey, parentKey):
        return currKey > parentKey

    def bubbleDown(self, currInd, lChildInd, rChildInd):
        largestInd = currInd

        if lChildInd >= self.size():
            return currInd
        elif rChildInd >= self.size():
            largestInd = lChildInd
        else:
            largestInd = lChildInd if self.array[lChildInd] > self.array[rChildInd] else rChildInd

        if self.array[currInd] < self.array[largestInd]:
            self.array[currInd], self.array[largestInd] = \
                self.array[largestInd], self.array[currInd]
        else:
            largestInd = currInd

        return largestInd


def calcSumOfMedians(filename):
    lowHeap = MaxHeap()
    highHeap = MinHeap()
    medSum = 0
    count = 0

    with open(filename, 'r') as infile:
        for line in infile:
            count += 1
            newElem = int(line.strip())
#            print "----->"
#            print "newElem: " + str(newElem)
#            print "a) lowHeap: " + str(lowHeap.size()) + ",  " + str(lowHeap.getFirst())
#            print "   highHeap: " + str(highHeap.size()) + ",  " + str(highHeap.getFirst())
            if newElem <= lowHeap.getFirst():
                lowHeap.insert(newElem)
            elif newElem > highHeap.getFirst():
                highHeap.insert(newElem)
            else:
                if highHeap.size() >= lowHeap.size():
                    lowHeap.insert(newElem)
                else:
                    highHeap.insert(newElem)
            
#            print "b) lowHeap: " + str(lowHeap.size()) + ",  " + str(lowHeap.getFirst())
#            print "   highHeap: " + str(highHeap.size()) + ",  " + str(highHeap.getFirst())

            while lowHeap.size() < highHeap.size():
                lowHeap.insert(highHeap.extractFirst())

#            print "c) lowHeap: " + str(lowHeap.size()) + ",  " + str(lowHeap.getFirst())
#            print "   highHeap: " + str(highHeap.size()) + ",  " + str(highHeap.getFirst())

            while lowHeap.size() - highHeap.size() >= 2:
                highHeap.insert(lowHeap.extractFirst())

#            print "d) lowHeap: " + str(lowHeap.size()) + ",  " + str(lowHeap.getFirst())
#            print "   highHeap: " + str(highHeap.size()) + ",  " + str(highHeap.getFirst())


            medSum += lowHeap.getFirst()
#            print str(count) + ".    " + str(lowHeap.getFirst())

#            print "<-----" 
    return medSum

if __name__ == "__main__":
    fileName = sys.argv[1]
    medSum = calcSumOfMedians(fileName)

    print "Sum of medians mod 10000: " + str(medSum % 10000)

