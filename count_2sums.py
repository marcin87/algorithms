import sys


class HashTable:
    def __init__(self, size):
        self.size = size
        self.array = []

        for i in xrange(size):
            self.array.append([])

    def hashIt(self, key):
        return abs(key) % self.size

    def insert(self, key):
        hashValue = self.hashIt(key)
        if not key in self.array[hashValue]:
            self.array[hashValue].insert(0, key)

    def find(self, key):
        return key in self.array[self.hashIt(key)] 

    def show(self):
        for i in xrange(len(self.array)):
            print "---bucket [" + str(i) + "]"
            for elem in self.array[i]:
                print "--> " + str(elem),
            
            print
            print "---------------"

class Range:
    def __init__(self, minVal, maxVal):
        self.min = minVal
        self.max = maxVal

def loadFile(fileName):
    print "Loading file ..."
    array = []

    with open(fileName, 'r') as f:
        for line in f:
            array.append(int(line.strip()))
    
    return array
    
def loadHashTable(array):
    print "Loading hashtable"
    hashTable = HashTable(len(array))

    for elem in array:
        hashTable.insert(elem)

    return hashTable

def count2Sums(array, hashTable, sumRange):
    print "Counting sums ..."
    count = 0
    progressCnt = 0
    
    for t in xrange(sumRange.min, sumRange.max + 1):
        progressCnt += 1
        sys.stdout.write("\rDone: %.2f%%" % (100 * progressCnt / (sumRange.max - sumRange.min)))
        sys.stdout.flush()
 
        for x in array:
            y = t - x

            if y == x:
                continue

            if hashTable.find(y):
                count += 1
                break
    
    print
    return count



if __name__ == "__main__":
    fileName = sys.argv[1]
    minVal = int(sys.argv[2])
    maxVal = int(sys.argv[3])

    array = loadFile(fileName)
    hashTable = loadHashTable(array)
    #hashTable.show()
    sumRange = Range(minVal, maxVal)
    sumsCount = count2Sums(array, hashTable, sumRange)

    print "Sums count: " + str(sumsCount)

