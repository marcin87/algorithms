import sys

def multiply(strFirst, strSecond):
    first = None
    second = None
    
    firstLen = len(strFirst)
    secondLen = len(strSecond)
    maxSize = firstLen

    maxSize = max(firstLen, secondLen)
    if(maxSize % 2 != 0 and maxSize > 1):
        maxSize += 1
    
    while(firstLen < maxSize):
        strFirst = "0" + strFirst
        firstLen += 1

    while(secondLen < maxSize):
        strSecond = "0" + strSecond
        secondLen += 1

    size = maxSize

    if size <= 1:
        return int(strFirst) * int(strSecond)

    mid = len(strFirst)/2

    a = strFirst[:mid]
    b = strFirst[mid:]
    c = strSecond[:mid]
    d = strSecond[mid:]

    ac = multiply(a,c)
    bd = multiply(b,d)
    ab_sum = str(int(a) + int(b))
    cd_sum = str(int(c) + int(d))
    
    ab_sum_times_cd_sum = multiply(ab_sum, cd_sum)
    ad_bc_sum = ab_sum_times_cd_sum - ac - bd

    return (10**size)*ac + (10**(size/2))*ad_bc_sum + bd

if __name__ == "__main__":
    if len(sys.argv) != 3:
        raise Exception("Invalid number of arguments!")

    print multiply(sys.argv[1], sys.argv[2])
